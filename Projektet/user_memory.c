
/*
  Memory layout:
  32 bytes per user
  Byte 0-15 Name
  Byte 16 Password
  Byte 17 Tickets left




*/
const char MEMORY_SIZE = 128;
const char USER_SIZE = 16;
const char MAX_INDEX = MEMORY_SIZE/USER_SIZE;

void getName(char index, char * output){
  for(char i = 0; i < 8; i++){
    char row = index * USER_SIZE + i;
    output[i] = getchar_eedata(row);
    if(output[i] == 0) return;
  }
}

char getPassword(char index){
  char row = index*USER_SIZE + 16;
  return getchar_eedata(row);
}

char getTickets(char index){
  char row = index*USER_SIZE + 10;
  return getchar_eedata(row);
}

void setName(char index, const char * input){
  for(char i = 0; i < 15; i++){
    if(input[i] == 0) return;
    char row = index * USER_SIZE + i;
    putchar_eedata(row, input[i])
  }
}

void setPassword(char index, char input){
  char row = index*USER_SIZE + 16;
  putchar_eedata(row, input);
}

void setTickets(char index, char input){
  char row = index*USER_SIZE + 17;
  putchar_eedata(row, input);
}

char validIndex(char index){
   if(index <= MAX_INDEX)
    return 1;
   else
    return 0;
}
