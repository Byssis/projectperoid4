/* smartlock.c  question and answer, compare strings and unlock */
/* By: Albin Byström and Thomas Peterson - Cc5x C-compiler - not ANSI-C*/

#include "16F690.h"
#include "int16Cxx.h"
#pragma config |= 0x00D4
#define MAX_STRING 16

#define MEMORY_SIZE 128
#define USER_SIZE 16
#define MAX_INDEX 8

//----------Index of functions----------

//Data processing
void displayIntro();
void processCardData(char *, char *, char *);
void animate(const char *);

//Communication
void initserial( void );
void OverrunRecover(void);
void putchar( char );
char getchar( void );     /* Special! nonblocking when card removed  */
void string_in( char * ); /* no extra echo, local echo by connection */
void string_out( const char *);
void printf(const char *string, char variable);

//Time functions
void initTimer();
void displayTime();

//Memory functions
void getName(char index, char * output);
void getPassword(char index, char * output);
char getTickets(char index);
void setName(char index, char * input);
void setPassword(char index, char * input);
void setTickets(char index, char input);
char validIndex(char index);
void putchar_eedata( char data, char adress );
char getchar_eedata( char adress );

//Memory processing functions
void resetMemory();
void printMemory();

//Helper functions
bit check_string( char *, const char *);
void delay( char );
void delaySeconds( char );
void NBCDToChar(char,char *);
char CharToNBCD(char,char);

//Test functions
void testValidIndex();
//--------------------------------------


char tosec, toggle;
char Seconds, Minutes, Hours, Days, Month, Year;
unsigned long time;

// Interrupt Handler
#pragma origin 4
interrupt int_server( void )
{
	// ********** Interrupt **********
	int_save_registers
	// Timer2 Interrupt- Freq = 35.01 Hz
	if (PIR1.1 == 1)          // timer 2 interrupt flag
	{
		// ********** Tick **********
		tosec++;
		// ********** Update time **********
		if(tosec == 35){
			time++;
			tosec = 0;
			Seconds++;
			 if (Seconds == 60) {Minutes++; Seconds=0;
				if (Minutes == 60) {Hours++; Minutes=0;
				  if (Hours == 24) {Days++; Hours=0;
					if (  (Days == 29 && Month==2 && Year%4 == 0)                        	// February in leap year
					   || (Days == 30 && Month==2)                                          // February in normal years
					   || (Days == 31 && (Month==4 || Month==6 || Month==9 || Month==11 ))  // All months with 30 days
					   || (Days == 32)                                                      // All months with 31 days
					   ) {Month++; Days=1;}
					if (Month == 13) {Year++; Month=1;}
					}
				}
			}
		}
		PIR1.1 = 0;     // clears TMR2IF
	}
	int_restore_registers
}

void main( void){
   char input_string[MAX_STRING];
   char cardName[MAX_STRING];
   char cardPin[4];
   char pin[4];
	 bit doorOpen = 0;

   TRISA.0 = 1; /* RA0 not to disturb PK2 UART Tool */
   ANSEL.0 = 0; /* RA0 digital input */
   TRISA.1 = 1; /* RA1 not to disturb PK2 UART Tool */
   ANSEL.1 = 0; /* RA1 digital input */
   initserial();

   TRISC.3 = 1;  /* RC3 card contact is input       */
   ANSEL.7 = 0;  /* RC3 digital input               */
   TRISC.2 = 0;  /* RC2 lock (lightdiode) is output */
   PORTC.2 = 0;  /* RC2 initially locked            */

   initTimer();

   delaySeconds(2);//Wait for user to connect via UART

	 // ********** Intro **********
	 displayIntro();


	 // ********** Main **********
   while(1){

		 	// ********** Wait for card insertion **********
  		while( PORTC.3 == 0); /* wait for card insertion */

      delay(100); /* card debounce */
      delay(50);  /* extra delay   */

			// ********** Check card info **********
      /* ask the question */
      string_out( "who is it?\r\n");

      delay(100); /* USART is buffered, so wait until all chars sent  */

      /* empty the reciever FIFO, it's now full with garbage */
      OverrunRecover();

			// ********** Process input **********
     	/* get the answer string from the card */
      string_in( &input_string[0] );

      delay(100);
      delay(50);

      processCardData(&input_string[0],&cardName[0],&cardPin[0]);

			// ********** Check name **********
			int i;
			for(i = 0; i < MAX_INDEX; i++){
					getName(i,&input_string[0]);		// Reusing input_string to save memory
					if (check_string(&cardName[0], &input_string[0])){
						break;
					}
			 }
			 if(i >= MAX_INDEX){
				 // ********** Access Denied **********
				 	animate("Access Denied!");
			 }
			 else{
				 	// ********** Check pin **********
			 		getPassword(i,&input_string[0]);		// Reusing input_string to save memory
					if (check_string(&cardPin[0], &input_string[0])){
							// ********** Check tickets **********
							int tickets = getTickets(i);
							if(tickets > 0){
								// ********** Access Granted **********
								doorOpen = 1;
								animate("Access Granted!");
								tickets--;
								PORTC.2 = 1;
								setTickets(i, tickets);
								printf("\r\nTickets left: %d", tickets);
							}
							else
								// ********** Access Denied **********
								animate("No tickets left!\r\nAccess Denied!");
			    		}
					else{
						// ********** Access Denied **********
				    animate("Wrong Pin Number!");
				  }
			}
			displayTime();
			animate("\r\n");


     	//For debugging
      animate( "\r\n--Data Recieved--\r\n");

      animate(&cardName[0]);

      animate(":");

      animate(&cardPin[0]);//Cannot be sent via animate, encounters unkown bug
      delay(10); //Wait for all characters to be sent

      animate("\r\n");

      animate( "-----------------\r\n");
      animate("\r\n");

      delay(100);  /* extra delay */
			// ********** Command section **********
      OverrunRecover();
    	animate("[*] Waiting for commands\r\n");
      delay(100);

			// ********** While card is inserted **********
      while( PORTC.3 == 1){ /* wait for card removal */

		 		 OverrunRecover();
         delay(100);
         string_in( &input_string[0] );
         delay(100);
         animate("[*] Data recieved - \'");
		 	 	 //displayTime();
         animate(&input_string[0]);
         animate("\'\r\n");
				 // ********** Reset Memory **********
         if (check_string(&input_string[0],"reset memory")){
           animate("[*] Resetting memory\r\n");
					 resetMemory();
         }
				 // ********** Print Memory **********
				 else if (check_string(&input_string[0], "print memory" )){
					 printMemory();
				 }
      };

			animate("[*] Command session closed\r\n\r\n");
      delay(100);
			// ********** Lock **********

			// ********** If unlocked **********
			if (doorOpen == 1){
				 // ********** Hold open for 5s **********
				 animate("[*] Opening door for 5 seconds\r\n");
				 delaySeconds(5);//Hold the door open for 5 seconds after card removal
			}

			// ********** Lock **********
			doorOpen = 0;


      cardName[0] = 0; //removing old credentials
      cardPin[0] = 0;

      delay(10);


      delay(100);   /* card debounce */
    }
}




/********************
     FUNCTIONS
     =========
*********************/

//----------Data processing----------
void displayIntro(){
	/*
	Outputs the ascii art text "METRO SYSTEM" to UART
	*/
  animate(" __  __  _____  ____  _____  _____    _____ ___ ___ _____  ____  _____  __  __\r\n");
  animate("/  \\/  \\/   __\\/    \\/  _  \\/  _  \\  /  ___>\\  |  //  ___>/    \\/   __\\/  \\/  |\r\n");
  animate("|  \\/  ||   __|\\-  -/|  _  <|  |  |  |___  | |   | |___  |\\-  -/|   __||  \\/  |\r\n");
  animate("\\__ \\__/\\_____/ |__| \\__|\\_/\\_____/  <_____/ \\___/ <_____/ |__| \\_____/\\__ \\__/\r\n");
  animate("\r\n");
}

void processCardData(char * input, char * name, char * pin){
  /*
  Param input: Adress to the start of the input string
  Param name: Adress to the name string to be filled
  Parram pin: Adress to the pin string to be filled
  Splits the input string at ':'. The first part is stored in the name string and the second part is stored in the pin
  */
  char i;
  char k = 0;
  char temp;
  bit atPin = 0;
  char *target = name;
  for(i=0; ; i++)
    {
      if(input[i] == ':') {
        k = i+1;
        atPin = 1;
        target[i] = '\0';
        target = pin;
      }
      else{
        temp = input[i];
        target[i-k] = temp;
      }
      if( input[i] == '\0' ) return; /* exact match */
    }

}



void animate(const char * string){
  /*
  Param string: The string to be animated
  Sends portions of the string from the caller to the stringout function with small delays between each character
  */
  long char * text[2];//Regular char was not enough to hold the adress, long is used instead
  text[1] = 0;
  while(string[0] != 0){
    text[0] = string[0];
    string_out(&text[0]);
    string++;
    delay(10);
  }
}

//---Communication---
void initserial( void ){//COPIED from example files
   /*
   initialise PIC16F690 serialcom port
   */

   /* One start bit, one stop bit, 8 data bit, no parity. 9600 Baud. */

   TXEN = 1;      /* transmit enable                   */
   SYNC = 0;      /* asynchronous operation            */
   TX9  = 0;      /* 8 bit transmission                */
   SPEN = 1;

   BRGH  = 0;     /* settings for 6800 Baud            */
   BRG16 = 1;     /* @ 4 MHz-clock frequency           */
   SPBRG = 25;

   CREN = 1;      /* Continuous receive                */
   RX9  = 0;      /* 8 bit reception                   */
   ANSELH.3 = 0;  /* RB5 digital input for serial_in   */
}


void OverrunRecover(void){//COPIED from example files
  /*
  Empties the EUSART reciever
  */

   char trash;
   trash = RCREG;
   trash = RCREG;
   CREN = 0;
   CREN = 1;
}


void putchar( char d_out ){//COPIED from example files
  /*
  Param d_out: char to send
  Sends one char over EUSART
  */
   while (!TXIF) ;   /* wait until previus character transmitted   */
   TXREG = d_out;
   return; /* done */
}

char getchar( void ){//COPIED from example files
  /*
  Recieves one char from EUSART
  Returns: The recieved char
  */
   char d_in = '\r';
   while ( !RCIF && PORTC.3 ) ;  /* wait for character or card removal */
   if(!RCIF) return d_in;
   d_in = RCREG;
   return d_in;
}

void string_in( char * string ){//COPIED from example files
  /*
  Param string: The adress where the input data stored
  Gets the next input string from the UART communication
  */
   char charCount, c;
   for( charCount = 0; ; charCount++ )
       {
         c = getchar( );        /* input 1 character         */
         string[charCount] = c; /* store the character       */
         // putchar( c );       /* don't echo the character  */
         if( (charCount == (MAX_STRING-1))||(c=='\r' )) /* end of input */
           {
             string[charCount] = '\0'; /* add "end of string" */
             return;
           }
       }
}


void string_out(const char * string ){//COPIED from example files
  /*
  Param string: The adress to the string to be sent as output
  Sends a string as output to UART
  */
  char i, k;
  for(i = 0 ; ; i++)
   {
     k = string[i];
     if( k == '\0') return;   /* found end of string */
     putchar(k);
   }
  return;
}

void printf(const char *string, char variable){//COPIED from example files with minor changes
	/*
	Param string: The adress to the string to be sent over UART
	Param variable: An optional variable to add to the string
	Outputs the string and the variable to UART
	*/
  char i, k, m, a, b;
  char first = 1;
  for(i = 0 ; ; i++)
   {
     k = string[i];
     if( k == '\0') break;   // at end of string
     if( k == '%')           // insert variable in string
      {
        i++;
        k = string[i];
        switch(k)
         {
           case 'd':         // %d  signed 8bit
             if( variable.7 ==1) putchar('-');
             //else putchar(' ');
             if( variable > 127) variable = -variable;  // no break!
           case 'u':         // %u unsigned 8bit
			 a = variable/100;
			 if(!a && first) first = 0;
			 else{
				putchar('0'+a); // print 100's
				first = 0;
			}
             b = variable%100;
             a = b/10;
			 if(!a && first) first = 0;
             else{
				putchar('0'+a); // print 100's
				first = 0;
			}
             a = b%10;
			 if(!a && first) first = 0;
             else{
				putchar('0'+a); // print 100's
				first = 0;
			}
             break;
           case 'b':         // %b BINARY 8bit
             for( m = 0 ; m < 8 ; m++ )
              {
                if (variable.7 == 1) putchar('1');
                else putchar('0');
                variable = rl(variable);
               }
              break;
           case 'c':         // %c  'char'
             putchar(variable);
             break;
           case '%':
             putchar('%');
             break;
           default:          // not implemented
             putchar('!');
         }
      }
      else putchar(k);
   }
}

#pragma codepage 1
//----------Time functions-----------
void initTimer(){
	/*
	Initialises the timer module and the interrupts used for measuring time
	*/
	time = 0; // Need to change this so it reads from memory
	tosec = 0;
	toggle = 0;

	Seconds = 0;
	Minutes = 59;
	Hours = 11;
	Days = 3;
	Month = 3;
	Year = 16;

	//Timer2 Registers Prescaler= 16 - TMR2 PostScaler = 7 - PR2 = 255 - Freq = 35.01 Hz
	T2CON |= 48;        // bits 6-3 Post scaler 1:1 thru 1:16
	T2CON.2 = 1;        // bit 2 turn timer2 on;
	T2CON.1 = 1;        // bits 1-0  Prescaler Rate Select bits
	T2CON.0 = 0;
	PR2 = 255;          // PR2 (Timer2 Match value)

	// Interrupt Registers
	INTCON = 0;          // clear the interrupt control register
	INTCON.5 = 0;        // bit5 TMR0 Overflow Interrupt Enable bit...0 = Disables the TMR0 interrupt
	PIR1.1 = 0;          // clear timer1 interupt flag TMR1IF
	PIE1.1 = 1;          // enable Timer2 interrupts
	INTCON.2 = 0;        // bit2 clear timer 0 interrupt flag
	GIE = 1;             // bit7 global interrupt enable
	PEIE = 1;            // bit6 Peripheral Interrupt Enable bit...1 = Enables all unmasked peripheral interrupts
}

void displayTime(){
	/*
	Displays the time
	*/
	printf("\r\n",0);
	printf("Date: 20%d-", Year);
	printf("%d-", Month);
	printf("%d ", Days);
	printf("Time: %d:", Hours);
	printf("%d:", Minutes);
	printf("%d\r\n", Seconds);
}

//--Memory functions--
void getName(char index, char * output){
	/*
	Param index: The index of the user
	Param output: The adress where the name will be stored
	Fetches a name from memory using a user index and writes it to the output adress
	*/
   char i;
  for(i = 0; i < 8; i++){
    char row = index * USER_SIZE + i;
	char data = getchar_eedata(row);
    output[i] = data;
    if(output[i] == 0) return;
  }
   output[i] = 0;
}

void getPassword(char index, char * output){
	/*
	Param index: The index of the user
	Param output: The adress where the password will be stored
	Fetches a password from memory using a user index and writes it to the output adress
	*/
	char i;
  for(i = 0; i < 2; i++){
    char row = index * USER_SIZE + i + 8;
		char data = getchar_eedata(row);

		NBCDToChar(data, &output[i*2]);//Convert the NBCD pin number to numbers in char form
    if(output[i] == 0) return;
  }
  output[i*2] = 0;//Add nullbyte
 }

char getTickets(char index){
	/*
	Param index: The index of the user
	Fetches the amount of tickets a user still has from memory using a user index
	Retruns: The amount of tickets left
	*/
  char row = index*USER_SIZE + 10;
  char data = getchar_eedata(row);
  return data;
}

void setName(char index, char * input){
	/*
	Param index: The index of the user
	Param input: The adress from where the name will be loaded
	Stores a user's name in memory
	*/
   char i;
  for(i = 0; i < 8; i++){
    char row = index * USER_SIZE + i;
    putchar_eedata(input[i], row);
	if(input[i] == 0) return;
  }
}

void setPassword(char index, char * input){
	/*
	Param index: The index of the user
	Param input: The adress from where the password(A pin number of four digits) to store will be loaded
	Stores a user's password in memory.(In NBCD form)
	*/
  char i;
  for(i = 0; i < 2; i++){
    char row = index * USER_SIZE + i + 8;
		char arg1 = input[i*2];
		char arg2 = input[i*2+1];
		char data;
		data = CharToNBCD(arg1,arg2);
    putchar_eedata(data, row);
	if(input[i] == 0) return;
  }
}

void setTickets(char index, char input){
	/*
	Param index: The index of the user
	Param input: The amount of tickets the user should have
	Stores a user's ticket amount in memory
	*/
  char row = index*USER_SIZE + 10;
  putchar_eedata(input, row);
}

char validIndex(char index){
	/*
	Param index: The index of the user
	Checks if the user index is a valid user index or not
	Returns: Valid or not valid
	*/
  if(index < MAX_INDEX && index >= 0)
    return 1;
   else
    return 0;
}

void putchar_eedata( char data, char adress ){//COPIED from example files
	/*
	Param data: A byte of data
	Param adress: The target adress where the data will be stored
	Puts a char in a specific EEPROM-adress
	*/
  /* Write EEPROM-data sequence                          */
  EEADR = adress;     /* EEPROM-data adress 0x00 => 0x40 */
  EEPGD = 0;          /* Data, not Program memory        */
  EEDATA = data;      /* data to be written              */
  WREN = 1;           /* write enable                    */
  EECON2 = 0x55;      /* first Byte in comandsequence    */
  EECON2 = 0xAA;      /* second Byte in comandsequence   */
  WR = 1;             /* write                           */
  while( EEIF == 0) ; /* wait for done (EEIF=1)          */
  WR = 0;
  WREN = 0;           /* write disable - safety first    */
  EEIF = 0;           /* Reset EEIF bit in software      */
  /* End of write EEPROM-data sequence                   */
}


char getchar_eedata( char adress ){//COPIED from example files
	/*
	Param adress: The target adress from where the data will be loaded
	Gets a char in a specific EEPROM-adress
	Returns: The retrieved char
	*/
  /* Start of read EEPROM-data sequence                */
  char temp;
  EEADR = adress;  /* EEPROM-data adress 0x00 => 0x40  */
  EEPGD = 0;       /* Data not Program -memory         */
  RD = 1;          /* Read                             */
  temp = EEDATA;
  RD = 0;
  return temp;     /* data to be read                  */
  /* End of read EEPROM-data sequence                  */
}

//----Memory processing functions-----

void resetMemory(){
	/*
	Resets the flash memory to standard values.
	*/
	char input_string[8];

	input_string[0] = 'T';
	input_string[1] = 'h';
	input_string[2] = 'o';
	input_string[3] = 'm';
	input_string[4] = 'a';
	input_string[5] = 's';
	input_string[6] = 0;
	setName(0,&input_string[0]);
	input_string[0] = '2';
	input_string[1] = '3';
	input_string[2] = '0';
	input_string[3] = '9';
	setPassword(0,&input_string[0]);
	setTickets(0,102);

	input_string[0] = 'A';
	input_string[1] = 'l';
	input_string[2] = 'b';
	input_string[3] = 'i';
	input_string[4] = 'n';
	input_string[5] = 0;
	setName(1,&input_string[0]);
	input_string[0] = '1';
	input_string[1] = '3';
	input_string[2] = '3';
	input_string[3] = '7';
	setPassword(1,&input_string[0]);
	setTickets(1,101);

	input_string[0] = 'M';
	input_string[1] = 'a';
	input_string[2] = 'r';
	input_string[3] = 't';
	input_string[4] = 'i';
	input_string[5] = 'n';
	input_string[6] = 0;
	setName(2,&input_string[0]);
	input_string[0] = '1';
	input_string[1] = '2';
	input_string[2] = '3';
	input_string[3] = '4';
	setPassword(2,&input_string[0]);
	setTickets(2,10);

	input_string[0] = 'N';
	input_string[1] = 'i';
	input_string[2] = 'l';
	input_string[3] = 's';
	input_string[4] = 0;
	setName(3,&input_string[0]);
	input_string[0] = '4';
	input_string[1] = '3';
	input_string[2] = '2';
	input_string[3] = '1';
	setPassword(3,&input_string[0]);
	setTickets(3,18);

	input_string[0] = 'K';
	input_string[1] = 'u';
	input_string[2] = 'r';
	input_string[3] = 't';
	input_string[4] = 0;
	setName(4,&input_string[0]);
	input_string[0] = '4';
	input_string[1] = '4';
	input_string[2] = '3';
	input_string[3] = '6';
	setPassword(4,&input_string[0]);
	setTickets(4,14);

	input_string[0] = 'O';
	input_string[1] = 'l';
	input_string[2] = 'l';
	input_string[3] = 'e';
	input_string[4] = 0;
	setName(5,&input_string[0]);
	input_string[0] = '7';
	input_string[1] = '0';
	input_string[2] = '7';
	input_string[3] = '6';
	setPassword(5,&input_string[0]);
	setTickets(5,49);
}

void printMemory(){
	/*
	Displays all the memory data in the UART console
	*/
	char input_string[MAX_STRING];
	animate("--------------------\r\n");
	animate("Memory content: \r\n");
	int i;
	int tickets;
	for (i=0;i < MAX_INDEX;i++){
		animate("Name: ");
		getName(i,&input_string[0]);
		animate(&input_string[0]);
		animate(":");
		getPassword(i,&input_string[0]);
		animate(&input_string[0]);
		animate(":");
		tickets = getTickets(i);
		printf("%d",tickets);
		animate("\r\n");
	}
	animate("--------------------\r\n");
}

//----------Helper functions----------
bit check_string( char * input_string, const char * candidate_string ){//COPIED from example files with minor modifications
  /*
  Param input_string: The first string
  Param candidate_string: The second string
  Compares two strings to check if they are identical
  Returns: bit - 1 if the strings does exactly match, 0 if not
  */
  char i, c, d;
  for(i=0; ; i++)
  {
    c = input_string[i];
    d = candidate_string[i];
    if(d != c ) return 0;       /* no match    */
    if( d == '\0' ) return 1; /* exact match */
  }
}

void delay( char millisec){//COPIED from example files
/*
  Param millisec: The amount of milliseconds to delay
  Delays a multiple of 1 milliseconds at 4 MHz
  using the TMR0 timer by B. Knudsen
*/

    OPTION = 2;  /* prescaler divide by 8        */
    do  {
        TMR0 = 0;
        while ( TMR0 < 125)   /* 125 * 8 = 1000  */
            ;
    } while ( -- millisec > 0);
}

void delaySeconds( char seconds)
{
  /*
  Param seconds: The amount of seconds to delay
  Delays the system's processing by x seconds
  */
  int i;
  for(i = 0; i < seconds*4; i++){//Delay one second
    delay(250);
  }
}

void NBCDToChar(char number,char *output){
	/*
	Param number: Two numbers in NBCD format
	Param output: Destination of the unpacked numbers
	Unpacks two NBCD numbers to a specified adress
	*/
	unsigned int num1 = number & 0b11110000;
	num1 = (num1>>4);
	output[0] = num1+48;
	unsigned int num2 = number & 0b00001111;
	output[1] = num2+48;
}

char CharToNBCD(char upperNumber,char lowerNumber){
	/*
	Param upperNumber: The first number to pack
	Param lowerNumber: The second number to pack
	Converts two numbers into NBCD format and returns the result
	Returns result: The numbers in NBCD format
	*/
	unsigned int num1 = (upperNumber-48)<<4;
	unsigned int num2 = (lowerNumber-48);
	unsigned int result = num1 | num2;
	return result;
}

//----------Test functions-----------

void testValidIndex(){
	/*
	Prints out all valid indexes
	*/
	int i = 0;
	char result = -1;
	for(i = -1; i <= 10; i++){
		 animate("\r\n");
		 printf("%d",i);
		 animate(":");
		 result = validIndex(i);
		 printf("%d",result);
	}
	animate("\r\n");
}

/********************
      HARDWARE
      ========
*********************/

/*
           ___________  ___________
          |           \/           |
    +5V---|Vdd      16F690      Vss|---GND
          |RA5        RA0/AN0/(PGD)|-x ->- (PK2Rx)
          |RA4            RA1/(PGC)|-x -<- (PK2Tx)
    SW1---|RA3/!MCLR/(Vpp)  RA2/INT|
          |RC5/CCP              RC0|
          |RC4                  RC1|
  CrdIn->-|RC3                  RC2|->-LED (lock)
          |RC6                  RB4|
          |RC7               RB5/Rx|-<-SerIn
 SerOut-<-|RB7/Tx               RB6|
          |________________________|

Card Oscillator OSC 4 MHz ELFA 74-560-07

Card contact
                   __ __ __
              +5V |C1|   C5| GND
                  |__|   __|
        !MCLR/PGM |C2|  |C6|
                  |__|  |__|
          OSC/PGC |C3|  |C7| IO/PGD
                  |__|  |__|
                  |C4|  |C8|
                  |__|__|__|
                Contact CrdIn

All communications are tied together:

                  +5
                   |    --K<A-- = diodes
                  10k
                   |
           CrdIO---+--1k---(PK2Rx)
                   +--A>K--(PK2Tx)
      SerOut--K<A--+-------SerIn
*/
