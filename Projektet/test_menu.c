#include "16F690.h"
#include "int16Cxx.h"
#pragma config |= 0x00D4
#define MAX_STRING 16

void init();
void initserial( void );
void delay(char);
void putchar( char );
char getchar( void );     /* Special! nonblocking when card removed  */
void OverrunRecover(void);
void string_in( char * ); /* no extra echo, local echo by connection */
void string_out( const char * string );
void printf(const char *string, char variable);
void timer_test(void);

long millisec;
char seconds;
char min;
char hour;
#pragma origin 4
interrupt int_server( void )  /*the place for the interrupt routine*/
{
  int_save_registers
  // Timer 2
  //printf("Welcome!\r\n",0);
  //PORTC.2 = 1;
  if(T2IF== 1){

    millisec++;
    if(millisec >= 1000){
      seconds++;
      millisec = 0;
      PORTC.2 = !PORTC.2;
    }
    if(seconds >= 60){
      seconds = 0;
      min++;
    }
    if(min >= 60){
      min = 0;
      hour++;
    }
    T2IF = 0;
  }
  int_restore_registers
}


void main (void){
  init();
  char input_string[MAX_STRING];
  PORTC.2 = 1;
  T2CON.2 = 1;
  delay(100);
  //timer_test();
  printf("Welcome!\r\n",0);
  printf("===============\r\n",0);
  printf("Menu:\r\n",0);
  printf("A: Timer test\r\n", 0);
  printf("B: List Users\r\n", 0);
  printf("C: Edit Users\r\n", 0);
  while(1){
    string_in(&input_string[0]);
    switch (input_string[0]) {
      case 'a':
      case 'A':
        //fp = 'B';
      break;
      case 'b':
      case 'B':
        //fp = 'N';
      break;
      case 'c':
      case 'C':
        //fp = 'N';
      break;
    }
  }

}

void timer_test(){
  char seconds = 10;
  long per_second = 1000;
//  char prescale_per_second = 4;

  PORTC.2 = 1;
  T2CON.2 = 1;

  // Only for test, should use interupt to
  // count instead of polling
  // Sorry for messy code!
  while(seconds > 0){
    while(per_second > 0){
      while(PIR1.1 == 0);
      per_second--;
    }
    per_second = 1000;
    seconds--;
  }

  PORTC.2 = 0;
}

// initialization
// ==============================================

void init(){
  TRISA.0 = 1; /* RA0 not to disturb PK2 UART Tool */
  ANSEL.0 = 0; /* RA0 digital input */
  TRISA.1 = 1; /* RA1 not to disturb PK2 UART Tool */
  ANSEL.1 = 0; /* RA1 digital input */
  initserial();

  TRISC.3 = 1;  /* RC3 card contact is input       */
  ANSEL.7 = 0;  /* RC3 digital input               */
  TRISC.2 = 0;  /* RC2 lock (lightdiode) is output */
  PORTC.2 = 0;  /* RC2 initially locked            */

  // Timer 2
  T2CON |= 0x3;
  //T2CON |= 0xF << 3;
  PR2 = 250;
  TMR2 = 0;

  PEIE = 1;
  PIE1.1 = 1;
  RABIE   = 1;   /* local interrupt enable  */
  GIE     = 1;   /* global interrupt enable */

}

void initserial( void ) /* initialise serialcom port */
{
  /* One start bit, one stop bit, 8 data bit, no parity. 9600 Baud. */

  TXEN = 1;      /* transmit enable                   */
  SYNC = 0;      /* asynchronous operation            */
  TX9  = 0;      /* 8 bit transmission                */
  SPEN = 1;

  BRGH  = 0;     /* settings for 6800 Baud            */
  BRG16 = 1;     /* @ 4 MHz-clock frequency           */
  SPBRG = 25;

  CREN = 1;      /* Continuous receive                */
  RX9  = 0;      /* 8 bit reception                   */
  ANSELH.3 = 0;  /* RB5 digital input for serial_in   */
}



// Utilities
// ==============================================

void delay( char millisec)
/*
  Delays a multiple of 1 milliseconds at 4 MHz
  using the TMR0 timer
*/
{
  OPTION = 2;  /* prescaler divide by 8        */
  do  {
      TMR0 = 0;
      while ( TMR0 < 125)   /* 125 * 8 = 1000  */
          ;
  } while ( -- millisec > 0);
}


void OverrunRecover(void)
{
   char trash;
   trash = RCREG;
   trash = RCREG;
   CREN = 0;
   CREN = 1;
}

void putchar( char d_out )  /* sends one char */
{
   while (!TXIF) ;   /* wait until previus character transmitted   */
   TXREG = d_out;
   return; /* done */
}

char getchar( void )  /* recieves one char */
{
   char d_in = '\r';
   while ( !RCIF && PORTC.3 ) ;  /* wait for character or card removal */
   if(!RCIF) return d_in;
   d_in = RCREG;
   return d_in;
}

void string_in( char * string )
{
   char charCount, c;
   for( charCount = 0; ; charCount++ )
       {
         c = getchar( );        /* input 1 character         */
         string[charCount] = c; /* store the character       */
         // putchar( c );       /* don't echo the character  */
         if( (charCount == (MAX_STRING-1))||(c=='\r' )) /* end of input */
           {
             string[charCount] = '\0'; /* add "end of string" */
             return;
           }
       }
}


void string_out(const char * string )
{
  char i, k;
  for(i = 0 ; ; i++)
   {
     k = string[i];
     if( k == '\0') return;   /* found end of string */
     putchar(k);
   }
  return;
}


void printf(const char *string, char variable)
{
  char i, k, m, a, b;
  for(i = 0 ; ; i++)
   {
     k = string[i];
     if( k == '\0') break;   // at end of string
     if( k == '%')           // insert variable in string
      {
        i++;
        k = string[i];
        switch(k)
         {
           case 'd':         // %d  signed 8bit
             if( variable.7 ==1) putchar('-');
             else putchar(' ');
             if( variable > 127) variable = -variable;  // no break!
           case 'u':         // %u unsigned 8bit
             a = variable/100;
             putchar('0'+a); // print 100's
             b = variable%100;
             a = b/10;
             putchar('0'+a); // print 10's
             a = b%10;
             putchar('0'+a); // print 1's
             break;
           case 'b':         // %b BINARY 8bit
             for( m = 0 ; m < 8 ; m++ )
              {
                if (variable.7 == 1) putchar('1');
                else putchar('0');
                variable = rl(variable);
               }
              break;
           case 'c':         // %c  'char'
             putchar(variable);
             break;
           case '%':
             putchar('%');
             break;
           default:          // not implemented
             putchar('!');
         }
      }
      else putchar(k);
   }
}
